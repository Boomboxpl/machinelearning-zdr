using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ResultDisplayer : MonoBehaviour
{
    public TextMeshProUGUI currentTime;
    public TextMeshProUGUI bestTime;
    public TextMeshProUGUI cumulativeReward;
    public TextMeshProUGUI collisionCount;
    public TextMeshProUGUI successRate;
    [SerializeField] ShipAgent agent;

    float currentBestTime = float.PositiveInfinity;

    void Awake()
    {
        agent.onFinished += AgentFinished;
        cumulativeReward.text = "";
    }

    void Update()
    {
        currentTime.text = TimeSpan.FromSeconds(Time.time - agent.startTime).ToString(@"mm\:ss");
        bestTime.text =  float.IsInfinity(currentBestTime) ? "?" : TimeSpan.FromSeconds(currentBestTime).ToString(@"mm\:ss");
        collisionCount.text = agent.colCount.ToString();
        successRate.text = $"{agent.succesCount}\\{agent.succesCount + agent.failureCount}";
    }

    private void AgentFinished()
    {
        float agentTime = Time.time - agent.startTime;
        if(agentTime < currentBestTime)
        {
            currentBestTime = agentTime;
        }
        cumulativeReward.text = agent.GetCumulativeReward().ToString();
    }
}
