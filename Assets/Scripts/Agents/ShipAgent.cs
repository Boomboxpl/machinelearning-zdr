using System;
using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class ShipAgent : Agent
{
    public event Action onFinished;

    [SerializeField] float scale = 10f;
    [SerializeField] float shipSpawnradius = 25f;
    [SerializeField] float targetSpawnRadius = 14f;
    [SerializeField] float cameraDist = 5f;
    [SerializeField] Transform target;
    [SerializeField] Collider gameField;
    [SerializeField] Vector2 rotationSpeed;
    [SerializeField] AsteroidSetter asteroidSetter;
    Rigidbody body;


    float startDistance;
    float lastDistance;


    public float startTime { get; private set;}
    public int colCount { get; private set; }
    public int succesCount { get; private set; }
    public int failureCount { get; private set; }

    bool isSucess;
    bool wasSucess;

    [SerializeField] bool nonRandom = false;
    Vector3 startPos;
    Vector3 startPosTar;

    void Awake()
    {
        body = GetComponent<Rigidbody>();
        isSucess = false;
        colCount = 0;
        succesCount = 0;
        failureCount = 0;

        startPos = transform.localPosition;
        startPosTar = target.localPosition;
    }

    void Update()
    {
        //transform.LookAt(target);
    }

    private void OnDrawGizmos()
    {
        Color color  = isSucess ? Color.green : Color.red;
        Gizmos.color = color;
        Gizmos.DrawSphere(transform.parent.position, 4f);
    }

    public override void OnEpisodeBegin()
    {
        if(wasSucess && isSucess)
        {
            isSucess=false;
            failureCount++;
        }
        wasSucess = isSucess;
        base.OnEpisodeBegin();

        var swapTarget = UnityEngine.Random.Range(0, 2) == 0 ? false : true;

        asteroidSetter?.SetAsteroids();
        if (nonRandom)
        {
            transform.localPosition = startPos;
            target.localPosition = startPosTar;
            return;
        }
        transform.localPosition = MathExtension.GetRandomSpherePosition(shipSpawnradius);

        target.localPosition = MathExtension.GetRandomSpherePosition(UnityEngine.Random.Range(0, targetSpawnRadius));
        startDistance = Vector3.Distance(transform.localPosition, target.localPosition);
        body.velocity = Vector3.zero;
        body.Sleep();
        lastDistance = startDistance;

        startTime = Time.time;

        if (swapTarget)
        {
            var tempPosition = transform.localPosition;
            transform.localPosition = target.localPosition;
            target.localPosition = tempPosition;
        
        }

        float dist = cameraDist;
        transform.LookAt(target.position + new Vector3(UnityEngine.Random.Range(-dist, dist), UnityEngine.Random.Range(-dist, dist), UnityEngine.Random.Range(-dist, dist)));
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        base.OnActionReceived(actions);
        var continous = actions.ContinuousActions;
        transform.Rotate(new Vector3(rotationSpeed.x * continous[2], rotationSpeed.y * continous[0], 0), Space.Self);
        body.AddRelativeForce(new Vector3(0, 0, scale * continous[1] * 10), ForceMode.Impulse);
        AddReward(-10/MaxStep);

        float currentDistance = Vector3.Distance(transform.localPosition, target.localPosition);
        float distanceDelta = currentDistance - lastDistance;
        lastDistance = currentDistance;
        float gain = -distanceDelta / startDistance;
        AddReward(gain);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continous = actionsOut.ContinuousActions;

        continous[0] = Input.GetAxisRaw("Horizontal");
        continous[1] = Input.GetAxisRaw("UpDown");
        continous[2] = Input.GetAxisRaw("Vertical");
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target.localPosition);
        sensor.AddObservation(transform.forward);
        sensor.AddObservation(transform.localRotation);
        sensor.AddObservation(body.velocity);
        sensor.AddObservation(Vector3.Distance(transform.localPosition, target.localPosition));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Asteroid"))
        {
            AddReward(-0.2f);
            colCount++;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == target)
        {
            float duration = Time.time - startTime;
            AddReward(10f);
            isSucess = true;
            wasSucess = false;
            onFinished?.Invoke();
            succesCount++;
            EndEpisode();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other == gameField)
        {
            isSucess=false;
            wasSucess=false;
            AddReward(-1f);
            failureCount++;
            onFinished?.Invoke();
            EndEpisode();
        }
    }
}
