using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearnGrid : MonoBehaviour
{
    public GameObject gridElement;
    public Vector2 gridElementSize;
    public Vector2Int gridDimension;

    public void Awake()
    {
        Setup();
    }

    public void Setup()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        for(int i = -gridDimension.y/2; i < gridDimension.y/2; i++)
        {
            for(int j = -gridDimension.x/2; j < gridDimension.x/2; j++)
            {
                var newEl = Instantiate(gridElement, transform);
                newEl.transform.localPosition = new Vector3(gridElementSize.x * j, 0, gridElementSize.y * i);
            }
        }
    }
}
