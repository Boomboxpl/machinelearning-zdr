using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSetter : MonoBehaviour
{
    [SerializeField] GameObject asteroidPrefab;
    [SerializeField] float radius;
    [SerializeField] int count;

    [ContextMenu("Setup")]
    public void SetAsteroids()
    {
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }

        StartCoroutine(DestroyAfterFrame());

        for (int i = 0; i < count; i++)
        {
            GameObject newAsteroid = Instantiate(asteroidPrefab, transform);
            newAsteroid.transform.localPosition = MathExtension.GetRandomSpherePosition(radius);
        }
    }

    IEnumerator DestroyAfterFrame()
    {
        yield return null;
        foreach(Transform child in transform)
        {
            if (!child.gameObject.activeSelf)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
