using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtension
{
    public static Vector3 GetRandomSpherePosition(float radius)
    {
        float randomAngle = Random.Range(0, 2 * Mathf.PI);
        float randomPitch = Random.Range(0, 2 * Mathf.PI);
        return new Vector3(Mathf.Sin(randomAngle), Mathf.Sin(randomPitch), Mathf.Cos(randomAngle)) * radius;
    }
}
